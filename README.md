# Trabalho 2 - Arquitetura e Organização de Computadores II - 2018/1
Prof. Maurício Lima Pilla - http://lups.inf.ufpel.edu.br/~pilla

## Dados do(a) aluno(a)

Declaro que o presente trabalho contém código desenvolvido exclusivamente por mim e que não foi compartilhado de nenhuma forma com terceiros a não ser o professor da disciplina. Compreendo que qualquer violação destes princípios será punida rigorosamente de acordo com o Regimento da UFPEL.

(Preencha com seus dados)

- Nome completo: Giordano Estêvão Borges Leal
- Username do Github: Giordano-Estevao
- Email @inf: gebleal@inf.ufpel.edu.br

## Descrição

A equipe de desenvolvimento do processador Beef Mark I está em dúvida sobre os seguintes parâmetros do projeto:

- Tamanho do bloco: 32 ou 64 bytes? 
- *Cache* de dados: 8 KiB 2-associativa com tempo de acesso de 1 ns ou 16 KiB mapeamento direto com tempo de acesso de 2 ns? 
- TLB: 4 entradas com tempo de acesso de 1 ns ou 8 entradas com tempo de acesso de 2 ns?

As demais configurações já foram decididas:

- Endereçamento a palavra de 32 bits (com endereços alinhados)
- *Cache* e TLB com política de substituição LRU (*Least Recently Used*)
- Escritas *writethrough*
- Palavras e endereços de 32 bits
- Memória com latência de 50 ns por palavra
- Tabela de páginas de um nível
- Páginas de 1 KiB
- Frequência de operação de 1 GHz


Desconsidere o problema de buscar e executar as instruções. 

Para resolver o problema, um simulador orientado a traços deverá ser implementado e o espaço de projeto explorado. Um simulador orientado a traços (ou _traces_) recebe como entrada uma configuração de hierarquia de memória e uma lista de endereços acessados (ou traço). A resposta consiste de uma série de estatísticas sobre os acessos realizados, como taxa de acertos e falhas em cada nível, número de ciclos necessários para todos os acessos.

Além de desenvolver o simulador, o aluno deverá projetar experimentos para explorar o espaço de projeto.  Considere que a aplicação principal do processador é realizar processamento de sinais, principalmente MAC (*Multiply and Add*). Em geral são dados três vetores, *a*, *b* e *c*. *a + b x c* é então armazenado em *a*. As entradas são geralmente apresentadas em três tamanhos (por vetor):

- 10 valores
- 512 valores
- 1024 valores

Não é preciso explorar todo o espaço, se houver uma boa justificativa para isso.

## Formato do traço

O arquivo de traço terá entradas no seguinte formato:

        R 00000000

O primeiro campo indica se há uma leitura (caracter 'R') ou uma escrita ('W'). Os segundo campo contém o endereço em formato hexadecimal. Por exemplo,

        R 00000001
        W 000000FF

Correspondem a, respectivamente, leitura do endereço 1 e escrita no endereço 255. 

O separador é sempre um espaço em branco, o fim de linha é marcado por '\n'.

## Entrada

A assinatura da função a ser implementada é:

        struct stats * sim(int tamanho_bloco, int tamanho_cache, int entradas_tlb, char * filename, char * stream);

Onde _filename_ é o nome do arquivo de traço. Caso _filename_ seja _null_, _stream_ é uma *string* que contém o traço no mesmo formato do arquivo.

## Saída

O resultado da simulação deve ser retornado como uma estrutura com os seguintes campos:

        struct stats{
            unsigned long hits; 
            unsigned long misses;
            unsigned long cycles; /* número total de ciclos da simulação */
        };

Em caso de erro (por exemplo, uma configuração impossível), a saída será _null_.

## Produtos

* Implementação (*sim.c*) -- 7 pontos 
* Casos de teses do(a) aluno(a) (*aluno.c* e traços no diretório *aluno*) -- 1 ponto
* Relatório em formato Markdown explicando as decisões de projeto tomadas (*report.md*) -- 2 pontos

## Cronograma

* Envio da versão final: _01/06/2018_ 


