CC=gcc

CFLAGS=-Wall -Wextra -Werror -std=c11 -O0 -g

all: grade

sim.o: sim.c


testme: sim.o test.c
	$(CC) $(CFLAGS) -o test sim.o test.c

grade: testme
	./testme

aluno: aluno.c 
	$(CC) $(CFLAGS) -o aluno sim.c 

run_aluno: aluno
	./aluno

clean:
	rm -rf *.o sim
